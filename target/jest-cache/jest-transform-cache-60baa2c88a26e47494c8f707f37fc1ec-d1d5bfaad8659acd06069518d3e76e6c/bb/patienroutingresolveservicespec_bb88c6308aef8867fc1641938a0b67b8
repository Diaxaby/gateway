c12d74dbfcb961d85ac18fa8126edcc2
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
jest.mock('@angular/router');
const testing_1 = require("@angular/core/testing");
const http_1 = require("@angular/common/http");
const testing_2 = require("@angular/common/http/testing");
const router_1 = require("@angular/router");
const rxjs_1 = require("rxjs");
const patien_model_1 = require("../patien.model");
const patien_service_1 = require("../service/patien.service");
const patien_routing_resolve_service_1 = require("./patien-routing-resolve.service");
describe('Service Tests', () => {
    describe('Patien routing resolve service', () => {
        let mockRouter;
        let mockActivatedRouteSnapshot;
        let routingResolveService;
        let service;
        let resultPatien;
        beforeEach(() => {
            testing_1.TestBed.configureTestingModule({
                imports: [testing_2.HttpClientTestingModule],
                providers: [router_1.Router, router_1.ActivatedRouteSnapshot],
            });
            mockRouter = testing_1.TestBed.inject(router_1.Router);
            mockActivatedRouteSnapshot = testing_1.TestBed.inject(router_1.ActivatedRouteSnapshot);
            routingResolveService = testing_1.TestBed.inject(patien_routing_resolve_service_1.PatienRoutingResolveService);
            service = testing_1.TestBed.inject(patien_service_1.PatienService);
            resultPatien = undefined;
        });
        describe('resolve', () => {
            it('should return IPatien returned by find', () => {
                // GIVEN
                service.find = jest.fn(id => rxjs_1.of(new http_1.HttpResponse({ body: { id } })));
                mockActivatedRouteSnapshot.params = { id: 123 };
                // WHEN
                routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
                    resultPatien = result;
                });
                // THEN
                expect(service.find).toBeCalledWith(123);
                expect(resultPatien).toEqual({ id: 123 });
            });
            it('should return new IPatien if id is not provided', () => {
                // GIVEN
                service.find = jest.fn();
                mockActivatedRouteSnapshot.params = {};
                // WHEN
                routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
                    resultPatien = result;
                });
                // THEN
                expect(service.find).not.toBeCalled();
                expect(resultPatien).toEqual(new patien_model_1.Patien());
            });
            it('should route to 404 page if data not found in server', () => {
                // GIVEN
                spyOn(service, 'find').and.returnValue(rxjs_1.of(new http_1.HttpResponse({ body: null })));
                mockActivatedRouteSnapshot.params = { id: 123 };
                // WHEN
                routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
                    resultPatien = result;
                });
                // THEN
                expect(service.find).toBeCalledWith(123);
                expect(resultPatien).toEqual(undefined);
                expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
            });
        });
    });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJmaWxlIjoiRDpcXFByb2pldHNcXEphdmFcXGdlc3Rob3BpdGFsR2F0ZXdheVxcc3JjXFxtYWluXFx3ZWJhcHBcXGFwcFxcZW50aXRpZXNcXHBhdGllblxccm91dGVcXHBhdGllbi1yb3V0aW5nLXJlc29sdmUuc2VydmljZS5zcGVjLnRzIiwibWFwcGluZ3MiOiI7O0FBQUEsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0FBRTdCLG1EQUFnRDtBQUNoRCwrQ0FBb0Q7QUFDcEQsMERBQXVFO0FBQ3ZFLDRDQUFpRTtBQUNqRSwrQkFBMEI7QUFFMUIsa0RBQWtEO0FBQ2xELDhEQUEwRDtBQUUxRCxxRkFBK0U7QUFFL0UsUUFBUSxDQUFDLGVBQWUsRUFBRSxHQUFHLEVBQUU7SUFDN0IsUUFBUSxDQUFDLGdDQUFnQyxFQUFFLEdBQUcsRUFBRTtRQUM5QyxJQUFJLFVBQWtCLENBQUM7UUFDdkIsSUFBSSwwQkFBa0QsQ0FBQztRQUN2RCxJQUFJLHFCQUFrRCxDQUFDO1FBQ3ZELElBQUksT0FBc0IsQ0FBQztRQUMzQixJQUFJLFlBQWlDLENBQUM7UUFFdEMsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNkLGlCQUFPLENBQUMsc0JBQXNCLENBQUM7Z0JBQzdCLE9BQU8sRUFBRSxDQUFDLGlDQUF1QixDQUFDO2dCQUNsQyxTQUFTLEVBQUUsQ0FBQyxlQUFNLEVBQUUsK0JBQXNCLENBQUM7YUFDNUMsQ0FBQyxDQUFDO1lBQ0gsVUFBVSxHQUFHLGlCQUFPLENBQUMsTUFBTSxDQUFDLGVBQU0sQ0FBQyxDQUFDO1lBQ3BDLDBCQUEwQixHQUFHLGlCQUFPLENBQUMsTUFBTSxDQUFDLCtCQUFzQixDQUFDLENBQUM7WUFDcEUscUJBQXFCLEdBQUcsaUJBQU8sQ0FBQyxNQUFNLENBQUMsNERBQTJCLENBQUMsQ0FBQztZQUNwRSxPQUFPLEdBQUcsaUJBQU8sQ0FBQyxNQUFNLENBQUMsOEJBQWEsQ0FBQyxDQUFDO1lBQ3hDLFlBQVksR0FBRyxTQUFTLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7UUFFSCxRQUFRLENBQUMsU0FBUyxFQUFFLEdBQUcsRUFBRTtZQUN2QixFQUFFLENBQUMsd0NBQXdDLEVBQUUsR0FBRyxFQUFFO2dCQUNoRCxRQUFRO2dCQUNSLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLFNBQUUsQ0FBQyxJQUFJLG1CQUFZLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNyRSwwQkFBMEIsQ0FBQyxNQUFNLEdBQUcsRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLENBQUM7Z0JBRWhELE9BQU87Z0JBQ1AscUJBQXFCLENBQUMsT0FBTyxDQUFDLDBCQUEwQixDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFFO29CQUMzRSxZQUFZLEdBQUcsTUFBTSxDQUFDO2dCQUN4QixDQUFDLENBQUMsQ0FBQztnQkFFSCxPQUFPO2dCQUNQLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN6QyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDNUMsQ0FBQyxDQUFDLENBQUM7WUFFSCxFQUFFLENBQUMsaURBQWlELEVBQUUsR0FBRyxFQUFFO2dCQUN6RCxRQUFRO2dCQUNSLE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUN6QiwwQkFBMEIsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO2dCQUV2QyxPQUFPO2dCQUNQLHFCQUFxQixDQUFDLE9BQU8sQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDM0UsWUFBWSxHQUFHLE1BQU0sQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsT0FBTztnQkFDUCxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsQ0FBQztnQkFDdEMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLHFCQUFNLEVBQUUsQ0FBQyxDQUFDO1lBQzdDLENBQUMsQ0FBQyxDQUFDO1lBRUgsRUFBRSxDQUFDLHNEQUFzRCxFQUFFLEdBQUcsRUFBRTtnQkFDOUQsUUFBUTtnQkFDUixLQUFLLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsU0FBRSxDQUFDLElBQUksbUJBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0UsMEJBQTBCLENBQUMsTUFBTSxHQUFHLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxDQUFDO2dCQUVoRCxPQUFPO2dCQUNQLHFCQUFxQixDQUFDLE9BQU8sQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDM0UsWUFBWSxHQUFHLE1BQU0sQ0FBQztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsT0FBTztnQkFDUCxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDekMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDeEMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDNUQsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDLENBQUMiLCJuYW1lcyI6W10sInNvdXJjZXMiOlsiRDpcXFByb2pldHNcXEphdmFcXGdlc3Rob3BpdGFsR2F0ZXdheVxcc3JjXFxtYWluXFx3ZWJhcHBcXGFwcFxcZW50aXRpZXNcXHBhdGllblxccm91dGVcXHBhdGllbi1yb3V0aW5nLXJlc29sdmUuc2VydmljZS5zcGVjLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImplc3QubW9jaygnQGFuZ3VsYXIvcm91dGVyJyk7XG5cbmltcG9ydCB7IFRlc3RCZWQgfSBmcm9tICdAYW5ndWxhci9jb3JlL3Rlc3RpbmcnO1xuaW1wb3J0IHsgSHR0cFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgSHR0cENsaWVudFRlc3RpbmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cC90ZXN0aW5nJztcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBvZiB9IGZyb20gJ3J4anMnO1xuXG5pbXBvcnQgeyBJUGF0aWVuLCBQYXRpZW4gfSBmcm9tICcuLi9wYXRpZW4ubW9kZWwnO1xuaW1wb3J0IHsgUGF0aWVuU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2UvcGF0aWVuLnNlcnZpY2UnO1xuXG5pbXBvcnQgeyBQYXRpZW5Sb3V0aW5nUmVzb2x2ZVNlcnZpY2UgfSBmcm9tICcuL3BhdGllbi1yb3V0aW5nLXJlc29sdmUuc2VydmljZSc7XG5cbmRlc2NyaWJlKCdTZXJ2aWNlIFRlc3RzJywgKCkgPT4ge1xuICBkZXNjcmliZSgnUGF0aWVuIHJvdXRpbmcgcmVzb2x2ZSBzZXJ2aWNlJywgKCkgPT4ge1xuICAgIGxldCBtb2NrUm91dGVyOiBSb3V0ZXI7XG4gICAgbGV0IG1vY2tBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90OiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90O1xuICAgIGxldCByb3V0aW5nUmVzb2x2ZVNlcnZpY2U6IFBhdGllblJvdXRpbmdSZXNvbHZlU2VydmljZTtcbiAgICBsZXQgc2VydmljZTogUGF0aWVuU2VydmljZTtcbiAgICBsZXQgcmVzdWx0UGF0aWVuOiBJUGF0aWVuIHwgdW5kZWZpbmVkO1xuXG4gICAgYmVmb3JlRWFjaCgoKSA9PiB7XG4gICAgICBUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUoe1xuICAgICAgICBpbXBvcnRzOiBbSHR0cENsaWVudFRlc3RpbmdNb2R1bGVdLFxuICAgICAgICBwcm92aWRlcnM6IFtSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlU25hcHNob3RdLFxuICAgICAgfSk7XG4gICAgICBtb2NrUm91dGVyID0gVGVzdEJlZC5pbmplY3QoUm91dGVyKTtcbiAgICAgIG1vY2tBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90ID0gVGVzdEJlZC5pbmplY3QoQWN0aXZhdGVkUm91dGVTbmFwc2hvdCk7XG4gICAgICByb3V0aW5nUmVzb2x2ZVNlcnZpY2UgPSBUZXN0QmVkLmluamVjdChQYXRpZW5Sb3V0aW5nUmVzb2x2ZVNlcnZpY2UpO1xuICAgICAgc2VydmljZSA9IFRlc3RCZWQuaW5qZWN0KFBhdGllblNlcnZpY2UpO1xuICAgICAgcmVzdWx0UGF0aWVuID0gdW5kZWZpbmVkO1xuICAgIH0pO1xuXG4gICAgZGVzY3JpYmUoJ3Jlc29sdmUnLCAoKSA9PiB7XG4gICAgICBpdCgnc2hvdWxkIHJldHVybiBJUGF0aWVuIHJldHVybmVkIGJ5IGZpbmQnLCAoKSA9PiB7XG4gICAgICAgIC8vIEdJVkVOXG4gICAgICAgIHNlcnZpY2UuZmluZCA9IGplc3QuZm4oaWQgPT4gb2YobmV3IEh0dHBSZXNwb25zZSh7IGJvZHk6IHsgaWQgfSB9KSkpO1xuICAgICAgICBtb2NrQWN0aXZhdGVkUm91dGVTbmFwc2hvdC5wYXJhbXMgPSB7IGlkOiAxMjMgfTtcblxuICAgICAgICAvLyBXSEVOXG4gICAgICAgIHJvdXRpbmdSZXNvbHZlU2VydmljZS5yZXNvbHZlKG1vY2tBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90KS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcbiAgICAgICAgICByZXN1bHRQYXRpZW4gPSByZXN1bHQ7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIFRIRU5cbiAgICAgICAgZXhwZWN0KHNlcnZpY2UuZmluZCkudG9CZUNhbGxlZFdpdGgoMTIzKTtcbiAgICAgICAgZXhwZWN0KHJlc3VsdFBhdGllbikudG9FcXVhbCh7IGlkOiAxMjMgfSk7XG4gICAgICB9KTtcblxuICAgICAgaXQoJ3Nob3VsZCByZXR1cm4gbmV3IElQYXRpZW4gaWYgaWQgaXMgbm90IHByb3ZpZGVkJywgKCkgPT4ge1xuICAgICAgICAvLyBHSVZFTlxuICAgICAgICBzZXJ2aWNlLmZpbmQgPSBqZXN0LmZuKCk7XG4gICAgICAgIG1vY2tBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LnBhcmFtcyA9IHt9O1xuXG4gICAgICAgIC8vIFdIRU5cbiAgICAgICAgcm91dGluZ1Jlc29sdmVTZXJ2aWNlLnJlc29sdmUobW9ja0FjdGl2YXRlZFJvdXRlU25hcHNob3QpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xuICAgICAgICAgIHJlc3VsdFBhdGllbiA9IHJlc3VsdDtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gVEhFTlxuICAgICAgICBleHBlY3Qoc2VydmljZS5maW5kKS5ub3QudG9CZUNhbGxlZCgpO1xuICAgICAgICBleHBlY3QocmVzdWx0UGF0aWVuKS50b0VxdWFsKG5ldyBQYXRpZW4oKSk7XG4gICAgICB9KTtcblxuICAgICAgaXQoJ3Nob3VsZCByb3V0ZSB0byA0MDQgcGFnZSBpZiBkYXRhIG5vdCBmb3VuZCBpbiBzZXJ2ZXInLCAoKSA9PiB7XG4gICAgICAgIC8vIEdJVkVOXG4gICAgICAgIHNweU9uKHNlcnZpY2UsICdmaW5kJykuYW5kLnJldHVyblZhbHVlKG9mKG5ldyBIdHRwUmVzcG9uc2UoeyBib2R5OiBudWxsIH0pKSk7XG4gICAgICAgIG1vY2tBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LnBhcmFtcyA9IHsgaWQ6IDEyMyB9O1xuXG4gICAgICAgIC8vIFdIRU5cbiAgICAgICAgcm91dGluZ1Jlc29sdmVTZXJ2aWNlLnJlc29sdmUobW9ja0FjdGl2YXRlZFJvdXRlU25hcHNob3QpLnN1YnNjcmliZShyZXN1bHQgPT4ge1xuICAgICAgICAgIHJlc3VsdFBhdGllbiA9IHJlc3VsdDtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gVEhFTlxuICAgICAgICBleHBlY3Qoc2VydmljZS5maW5kKS50b0JlQ2FsbGVkV2l0aCgxMjMpO1xuICAgICAgICBleHBlY3QocmVzdWx0UGF0aWVuKS50b0VxdWFsKHVuZGVmaW5lZCk7XG4gICAgICAgIGV4cGVjdChtb2NrUm91dGVyLm5hdmlnYXRlKS50b0hhdmVCZWVuQ2FsbGVkV2l0aChbJzQwNCddKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9KTtcbn0pO1xuIl0sInZlcnNpb24iOjN9