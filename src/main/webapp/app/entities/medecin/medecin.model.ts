import * as dayjs from 'dayjs';
import { IPatien } from 'app/entities/patien/patien.model';

export interface IMedecin {
  id?: number;
  nomCompletMed?: string;
  emailMed?: string;
  telephoneMed?: string;
  adresseMed?: string | null;
  dateNaissancePat?: dayjs.Dayjs;
  patiens?: IPatien[] | null;
}

export class Medecin implements IMedecin {
  constructor(
    public id?: number,
    public nomCompletMed?: string,
    public emailMed?: string,
    public telephoneMed?: string,
    public adresseMed?: string | null,
    public dateNaissancePat?: dayjs.Dayjs,
    public patiens?: IPatien[] | null
  ) {}
}

export function getMedecinIdentifier(medecin: IMedecin): number | undefined {
  return medecin.id;
}
