import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMedecin, getMedecinIdentifier } from '../medecin.model';

export type EntityResponseType = HttpResponse<IMedecin>;
export type EntityArrayResponseType = HttpResponse<IMedecin[]>;

@Injectable({ providedIn: 'root' })
export class MedecinService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/medecins');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(medecin: IMedecin): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(medecin);
    return this.http
      .post<IMedecin>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(medecin: IMedecin): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(medecin);
    return this.http
      .put<IMedecin>(`${this.resourceUrl}/${getMedecinIdentifier(medecin) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(medecin: IMedecin): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(medecin);
    return this.http
      .patch<IMedecin>(`${this.resourceUrl}/${getMedecinIdentifier(medecin) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMedecin>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMedecin[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMedecinToCollectionIfMissing(medecinCollection: IMedecin[], ...medecinsToCheck: (IMedecin | null | undefined)[]): IMedecin[] {
    const medecins: IMedecin[] = medecinsToCheck.filter(isPresent);
    if (medecins.length > 0) {
      const medecinCollectionIdentifiers = medecinCollection.map(medecinItem => getMedecinIdentifier(medecinItem)!);
      const medecinsToAdd = medecins.filter(medecinItem => {
        const medecinIdentifier = getMedecinIdentifier(medecinItem);
        if (medecinIdentifier == null || medecinCollectionIdentifiers.includes(medecinIdentifier)) {
          return false;
        }
        medecinCollectionIdentifiers.push(medecinIdentifier);
        return true;
      });
      return [...medecinsToAdd, ...medecinCollection];
    }
    return medecinCollection;
  }

  protected convertDateFromClient(medecin: IMedecin): IMedecin {
    return Object.assign({}, medecin, {
      dateNaissancePat: medecin.dateNaissancePat?.isValid() ? medecin.dateNaissancePat.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateNaissancePat = res.body.dateNaissancePat ? dayjs(res.body.dateNaissancePat) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((medecin: IMedecin) => {
        medecin.dateNaissancePat = medecin.dateNaissancePat ? dayjs(medecin.dateNaissancePat) : undefined;
      });
    }
    return res;
  }
}
