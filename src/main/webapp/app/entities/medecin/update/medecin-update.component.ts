import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IMedecin, Medecin } from '../medecin.model';
import { MedecinService } from '../service/medecin.service';

@Component({
  selector: 'jhi-medecin-update',
  templateUrl: './medecin-update.component.html',
})
export class MedecinUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nomCompletMed: [null, [Validators.required]],
    emailMed: [null, [Validators.required]],
    telephoneMed: [null, [Validators.required]],
    adresseMed: [],
    dateNaissancePat: [null, [Validators.required]],
  });

  constructor(protected medecinService: MedecinService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ medecin }) => {
      this.updateForm(medecin);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const medecin = this.createFromForm();
    if (medecin.id !== undefined) {
      this.subscribeToSaveResponse(this.medecinService.update(medecin));
    } else {
      this.subscribeToSaveResponse(this.medecinService.create(medecin));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMedecin>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(medecin: IMedecin): void {
    this.editForm.patchValue({
      id: medecin.id,
      nomCompletMed: medecin.nomCompletMed,
      emailMed: medecin.emailMed,
      telephoneMed: medecin.telephoneMed,
      adresseMed: medecin.adresseMed,
      dateNaissancePat: medecin.dateNaissancePat,
    });
  }

  protected createFromForm(): IMedecin {
    return {
      ...new Medecin(),
      id: this.editForm.get(['id'])!.value,
      nomCompletMed: this.editForm.get(['nomCompletMed'])!.value,
      emailMed: this.editForm.get(['emailMed'])!.value,
      telephoneMed: this.editForm.get(['telephoneMed'])!.value,
      adresseMed: this.editForm.get(['adresseMed'])!.value,
      dateNaissancePat: this.editForm.get(['dateNaissancePat'])!.value,
    };
  }
}
