import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'medecin',
        data: { pageTitle: 'gesthopitalGatewayApp.medecin.home.title' },
        loadChildren: () => import('./medecin/medecin.module').then(m => m.MedecinModule),
      },
      {
        path: 'patien',
        data: { pageTitle: 'gesthopitalGatewayApp.patien.home.title' },
        loadChildren: () => import('./patien/patien.module').then(m => m.PatienModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
