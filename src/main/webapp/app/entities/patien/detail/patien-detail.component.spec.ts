import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PatienDetailComponent } from './patien-detail.component';

describe('Component Tests', () => {
  describe('Patien Management Detail Component', () => {
    let comp: PatienDetailComponent;
    let fixture: ComponentFixture<PatienDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [PatienDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ patien: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(PatienDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PatienDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load patien on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.patien).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
