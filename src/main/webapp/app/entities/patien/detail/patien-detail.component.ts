import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPatien } from '../patien.model';

@Component({
  selector: 'jhi-patien-detail',
  templateUrl: './patien-detail.component.html',
})
export class PatienDetailComponent implements OnInit {
  patien: IPatien | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ patien }) => {
      this.patien = patien;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
