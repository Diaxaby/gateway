import * as dayjs from 'dayjs';
import { IMedecin } from 'app/entities/medecin/medecin.model';

export interface IPatien {
  id?: number;
  nomCompletPat?: string;
  emailPat?: string;
  telephonePat?: string;
  adressePat?: string | null;
  dateNaissancePat?: dayjs.Dayjs;
  medecins?: IMedecin[] | null;
}

export class Patien implements IPatien {
  constructor(
    public id?: number,
    public nomCompletPat?: string,
    public emailPat?: string,
    public telephonePat?: string,
    public adressePat?: string | null,
    public dateNaissancePat?: dayjs.Dayjs,
    public medecins?: IMedecin[] | null
  ) {}
}

export function getPatienIdentifier(patien: IPatien): number | undefined {
  return patien.id;
}
