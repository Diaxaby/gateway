import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PatienComponent } from '../list/patien.component';
import { PatienDetailComponent } from '../detail/patien-detail.component';
import { PatienUpdateComponent } from '../update/patien-update.component';
import { PatienRoutingResolveService } from './patien-routing-resolve.service';

const patienRoute: Routes = [
  {
    path: '',
    component: PatienComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PatienDetailComponent,
    resolve: {
      patien: PatienRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PatienUpdateComponent,
    resolve: {
      patien: PatienRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PatienUpdateComponent,
    resolve: {
      patien: PatienRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(patienRoute)],
  exports: [RouterModule],
})
export class PatienRoutingModule {}
