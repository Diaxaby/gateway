import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPatien, Patien } from '../patien.model';
import { PatienService } from '../service/patien.service';

@Injectable({ providedIn: 'root' })
export class PatienRoutingResolveService implements Resolve<IPatien> {
  constructor(protected service: PatienService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPatien> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((patien: HttpResponse<Patien>) => {
          if (patien.body) {
            return of(patien.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Patien());
  }
}
