import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IPatien, Patien } from '../patien.model';
import { PatienService } from '../service/patien.service';
import { IMedecin } from 'app/entities/medecin/medecin.model';
import { MedecinService } from 'app/entities/medecin/service/medecin.service';

@Component({
  selector: 'jhi-patien-update',
  templateUrl: './patien-update.component.html',
})
export class PatienUpdateComponent implements OnInit {
  isSaving = false;

  medecinsSharedCollection: IMedecin[] = [];

  editForm = this.fb.group({
    id: [],
    nomCompletPat: [null, [Validators.required]],
    emailPat: [null, [Validators.required]],
    telephonePat: [null, [Validators.required]],
    adressePat: [],
    dateNaissancePat: [null, [Validators.required]],
    medecins: [],
  });

  constructor(
    protected patienService: PatienService,
    protected medecinService: MedecinService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ patien }) => {
      this.updateForm(patien);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const patien = this.createFromForm();
    if (patien.id !== undefined) {
      this.subscribeToSaveResponse(this.patienService.update(patien));
    } else {
      this.subscribeToSaveResponse(this.patienService.create(patien));
    }
  }

  trackMedecinById(index: number, item: IMedecin): number {
    return item.id!;
  }

  getSelectedMedecin(option: IMedecin, selectedVals?: IMedecin[]): IMedecin {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPatien>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(patien: IPatien): void {
    this.editForm.patchValue({
      id: patien.id,
      nomCompletPat: patien.nomCompletPat,
      emailPat: patien.emailPat,
      telephonePat: patien.telephonePat,
      adressePat: patien.adressePat,
      dateNaissancePat: patien.dateNaissancePat,
      medecins: patien.medecins,
    });

    this.medecinsSharedCollection = this.medecinService.addMedecinToCollectionIfMissing(
      this.medecinsSharedCollection,
      ...(patien.medecins ?? [])
    );
  }

  protected loadRelationshipsOptions(): void {
    this.medecinService
      .query()
      .pipe(map((res: HttpResponse<IMedecin[]>) => res.body ?? []))
      .pipe(
        map((medecins: IMedecin[]) =>
          this.medecinService.addMedecinToCollectionIfMissing(medecins, ...(this.editForm.get('medecins')!.value ?? []))
        )
      )
      .subscribe((medecins: IMedecin[]) => (this.medecinsSharedCollection = medecins));
  }

  protected createFromForm(): IPatien {
    return {
      ...new Patien(),
      id: this.editForm.get(['id'])!.value,
      nomCompletPat: this.editForm.get(['nomCompletPat'])!.value,
      emailPat: this.editForm.get(['emailPat'])!.value,
      telephonePat: this.editForm.get(['telephonePat'])!.value,
      adressePat: this.editForm.get(['adressePat'])!.value,
      dateNaissancePat: this.editForm.get(['dateNaissancePat'])!.value,
      medecins: this.editForm.get(['medecins'])!.value,
    };
  }
}
