jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PatienService } from '../service/patien.service';
import { IPatien, Patien } from '../patien.model';
import { IMedecin } from 'app/entities/medecin/medecin.model';
import { MedecinService } from 'app/entities/medecin/service/medecin.service';

import { PatienUpdateComponent } from './patien-update.component';

describe('Component Tests', () => {
  describe('Patien Management Update Component', () => {
    let comp: PatienUpdateComponent;
    let fixture: ComponentFixture<PatienUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let patienService: PatienService;
    let medecinService: MedecinService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [PatienUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(PatienUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PatienUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      patienService = TestBed.inject(PatienService);
      medecinService = TestBed.inject(MedecinService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Medecin query and add missing value', () => {
        const patien: IPatien = { id: 456 };
        const medecins: IMedecin[] = [{ id: 27365 }];
        patien.medecins = medecins;

        const medecinCollection: IMedecin[] = [{ id: 11727 }];
        spyOn(medecinService, 'query').and.returnValue(of(new HttpResponse({ body: medecinCollection })));
        const additionalMedecins = [...medecins];
        const expectedCollection: IMedecin[] = [...additionalMedecins, ...medecinCollection];
        spyOn(medecinService, 'addMedecinToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ patien });
        comp.ngOnInit();

        expect(medecinService.query).toHaveBeenCalled();
        expect(medecinService.addMedecinToCollectionIfMissing).toHaveBeenCalledWith(medecinCollection, ...additionalMedecins);
        expect(comp.medecinsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const patien: IPatien = { id: 456 };
        const medecins: IMedecin = { id: 75897 };
        patien.medecins = [medecins];

        activatedRoute.data = of({ patien });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(patien));
        expect(comp.medecinsSharedCollection).toContain(medecins);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const patien = { id: 123 };
        spyOn(patienService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ patien });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: patien }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(patienService.update).toHaveBeenCalledWith(patien);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const patien = new Patien();
        spyOn(patienService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ patien });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: patien }));
        saveSubject.complete();

        // THEN
        expect(patienService.create).toHaveBeenCalledWith(patien);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const patien = { id: 123 };
        spyOn(patienService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ patien });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(patienService.update).toHaveBeenCalledWith(patien);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackMedecinById', () => {
        it('Should return tracked Medecin primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackMedecinById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });

    describe('Getting selected relationships', () => {
      describe('getSelectedMedecin', () => {
        it('Should return option if no Medecin is selected', () => {
          const option = { id: 123 };
          const result = comp.getSelectedMedecin(option);
          expect(result === option).toEqual(true);
        });

        it('Should return selected Medecin for according option', () => {
          const option = { id: 123 };
          const selected = { id: 123 };
          const selected2 = { id: 456 };
          const result = comp.getSelectedMedecin(option, [selected2, selected]);
          expect(result === selected).toEqual(true);
          expect(result === selected2).toEqual(false);
          expect(result === option).toEqual(false);
        });

        it('Should return option if this Medecin is not selected', () => {
          const option = { id: 123 };
          const selected = { id: 456 };
          const result = comp.getSelectedMedecin(option, [selected]);
          expect(result === option).toEqual(true);
          expect(result === selected).toEqual(false);
        });
      });
    });
  });
});
