import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { PatienComponent } from './list/patien.component';
import { PatienDetailComponent } from './detail/patien-detail.component';
import { PatienUpdateComponent } from './update/patien-update.component';
import { PatienDeleteDialogComponent } from './delete/patien-delete-dialog.component';
import { PatienRoutingModule } from './route/patien-routing.module';

@NgModule({
  imports: [SharedModule, PatienRoutingModule],
  declarations: [PatienComponent, PatienDetailComponent, PatienUpdateComponent, PatienDeleteDialogComponent],
  entryComponents: [PatienDeleteDialogComponent],
})
export class PatienModule {}
