import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPatien } from '../patien.model';
import { PatienService } from '../service/patien.service';

@Component({
  templateUrl: './patien-delete-dialog.component.html',
})
export class PatienDeleteDialogComponent {
  patien?: IPatien;

  constructor(protected patienService: PatienService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.patienService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
