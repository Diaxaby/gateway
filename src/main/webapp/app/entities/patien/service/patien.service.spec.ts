import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IPatien, Patien } from '../patien.model';

import { PatienService } from './patien.service';

describe('Service Tests', () => {
  describe('Patien Service', () => {
    let service: PatienService;
    let httpMock: HttpTestingController;
    let elemDefault: IPatien;
    let expectedResult: IPatien | IPatien[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(PatienService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        nomCompletPat: 'AAAAAAA',
        emailPat: 'AAAAAAA',
        telephonePat: 'AAAAAAA',
        adressePat: 'AAAAAAA',
        dateNaissancePat: currentDate,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateNaissancePat: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Patien', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateNaissancePat: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateNaissancePat: currentDate,
          },
          returnedFromService
        );

        service.create(new Patien()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Patien', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            nomCompletPat: 'BBBBBB',
            emailPat: 'BBBBBB',
            telephonePat: 'BBBBBB',
            adressePat: 'BBBBBB',
            dateNaissancePat: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateNaissancePat: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Patien', () => {
        const patchObject = Object.assign(
          {
            dateNaissancePat: currentDate.format(DATE_FORMAT),
          },
          new Patien()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            dateNaissancePat: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Patien', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            nomCompletPat: 'BBBBBB',
            emailPat: 'BBBBBB',
            telephonePat: 'BBBBBB',
            adressePat: 'BBBBBB',
            dateNaissancePat: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateNaissancePat: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Patien', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addPatienToCollectionIfMissing', () => {
        it('should add a Patien to an empty array', () => {
          const patien: IPatien = { id: 123 };
          expectedResult = service.addPatienToCollectionIfMissing([], patien);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(patien);
        });

        it('should not add a Patien to an array that contains it', () => {
          const patien: IPatien = { id: 123 };
          const patienCollection: IPatien[] = [
            {
              ...patien,
            },
            { id: 456 },
          ];
          expectedResult = service.addPatienToCollectionIfMissing(patienCollection, patien);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Patien to an array that doesn't contain it", () => {
          const patien: IPatien = { id: 123 };
          const patienCollection: IPatien[] = [{ id: 456 }];
          expectedResult = service.addPatienToCollectionIfMissing(patienCollection, patien);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(patien);
        });

        it('should add only unique Patien to an array', () => {
          const patienArray: IPatien[] = [{ id: 123 }, { id: 456 }, { id: 27472 }];
          const patienCollection: IPatien[] = [{ id: 123 }];
          expectedResult = service.addPatienToCollectionIfMissing(patienCollection, ...patienArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const patien: IPatien = { id: 123 };
          const patien2: IPatien = { id: 456 };
          expectedResult = service.addPatienToCollectionIfMissing([], patien, patien2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(patien);
          expect(expectedResult).toContain(patien2);
        });

        it('should accept null and undefined values', () => {
          const patien: IPatien = { id: 123 };
          expectedResult = service.addPatienToCollectionIfMissing([], null, patien, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(patien);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
