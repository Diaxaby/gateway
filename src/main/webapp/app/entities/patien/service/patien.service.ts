import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPatien, getPatienIdentifier } from '../patien.model';

export type EntityResponseType = HttpResponse<IPatien>;
export type EntityArrayResponseType = HttpResponse<IPatien[]>;

@Injectable({ providedIn: 'root' })
export class PatienService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/patiens');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(patien: IPatien): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(patien);
    return this.http
      .post<IPatien>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(patien: IPatien): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(patien);
    return this.http
      .put<IPatien>(`${this.resourceUrl}/${getPatienIdentifier(patien) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(patien: IPatien): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(patien);
    return this.http
      .patch<IPatien>(`${this.resourceUrl}/${getPatienIdentifier(patien) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPatien>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPatien[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPatienToCollectionIfMissing(patienCollection: IPatien[], ...patiensToCheck: (IPatien | null | undefined)[]): IPatien[] {
    const patiens: IPatien[] = patiensToCheck.filter(isPresent);
    if (patiens.length > 0) {
      const patienCollectionIdentifiers = patienCollection.map(patienItem => getPatienIdentifier(patienItem)!);
      const patiensToAdd = patiens.filter(patienItem => {
        const patienIdentifier = getPatienIdentifier(patienItem);
        if (patienIdentifier == null || patienCollectionIdentifiers.includes(patienIdentifier)) {
          return false;
        }
        patienCollectionIdentifiers.push(patienIdentifier);
        return true;
      });
      return [...patiensToAdd, ...patienCollection];
    }
    return patienCollection;
  }

  protected convertDateFromClient(patien: IPatien): IPatien {
    return Object.assign({}, patien, {
      dateNaissancePat: patien.dateNaissancePat?.isValid() ? patien.dateNaissancePat.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateNaissancePat = res.body.dateNaissancePat ? dayjs(res.body.dateNaissancePat) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((patien: IPatien) => {
        patien.dateNaissancePat = patien.dateNaissancePat ? dayjs(patien.dateNaissancePat) : undefined;
      });
    }
    return res;
  }
}
