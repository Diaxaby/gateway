package com.gesthopital.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.gesthopital.domain.Medecin;
import com.gesthopital.domain.Patien;
import com.gesthopital.repository.rowmapper.PatienRowMapper;
import com.gesthopital.service.EntityManager;
import com.gesthopital.service.EntityManager.LinkTable;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoin;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the Patien entity.
 */
@SuppressWarnings("unused")
class PatienRepositoryInternalImpl implements PatienRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final PatienRowMapper patienMapper;

    private static final Table entityTable = Table.aliased("patien", EntityManager.ENTITY_ALIAS);

    private static final EntityManager.LinkTable medecinLink = new LinkTable("rel_patien__medecin", "patien_id", "medecin_id");

    public PatienRepositoryInternalImpl(R2dbcEntityTemplate template, EntityManager entityManager, PatienRowMapper patienMapper) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.patienMapper = patienMapper;
    }

    @Override
    public Flux<Patien> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<Patien> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<Patien> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = PatienSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        SelectFromAndJoin selectFrom = Select.builder().select(columns).from(entityTable);

        String select = entityManager.createSelect(selectFrom, Patien.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(
                crit ->
                    new StringBuilder(select)
                        .append(" ")
                        .append("WHERE")
                        .append(" ")
                        .append(alias)
                        .append(".")
                        .append(crit.toString())
                        .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<Patien> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<Patien> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    @Override
    public Mono<Patien> findOneWithEagerRelationships(Long id) {
        return findById(id);
    }

    @Override
    public Flux<Patien> findAllWithEagerRelationships() {
        return findAll();
    }

    @Override
    public Flux<Patien> findAllWithEagerRelationships(Pageable page) {
        return findAllBy(page);
    }

    private Patien process(Row row, RowMetadata metadata) {
        Patien entity = patienMapper.apply(row, "e");
        return entity;
    }

    @Override
    public <S extends Patien> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends Patien> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity).flatMap(savedEntity -> updateRelations(savedEntity));
        } else {
            return update(entity)
                .map(
                    numberOfUpdates -> {
                        if (numberOfUpdates.intValue() <= 0) {
                            throw new IllegalStateException("Unable to update Patien with id = " + entity.getId());
                        }
                        return entity;
                    }
                )
                .then(updateRelations(entity));
        }
    }

    @Override
    public Mono<Integer> update(Patien entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }

    @Override
    public Mono<Void> deleteById(Long entityId) {
        return deleteRelations(entityId)
            .then(r2dbcEntityTemplate.delete(Patien.class).matching(query(where("id").is(entityId))).all().then());
    }

    protected <S extends Patien> Mono<S> updateRelations(S entity) {
        Mono<Void> result = entityManager
            .updateLinkTable(medecinLink, entity.getId(), entity.getMedecins().stream().map(Medecin::getId))
            .then();
        return result.thenReturn(entity);
    }

    protected Mono<Void> deleteRelations(Long entityId) {
        return entityManager.deleteFromLinkTable(medecinLink, entityId);
    }
}

class PatienSqlHelper {

    static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("nom_complet_pat", table, columnPrefix + "_nom_complet_pat"));
        columns.add(Column.aliased("email_pat", table, columnPrefix + "_email_pat"));
        columns.add(Column.aliased("telephone_pat", table, columnPrefix + "_telephone_pat"));
        columns.add(Column.aliased("adresse_pat", table, columnPrefix + "_adresse_pat"));
        columns.add(Column.aliased("date_naissance_pat", table, columnPrefix + "_date_naissance_pat"));

        return columns;
    }
}
