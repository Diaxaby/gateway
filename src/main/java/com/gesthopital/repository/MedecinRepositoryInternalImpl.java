package com.gesthopital.repository;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

import com.gesthopital.domain.Medecin;
import com.gesthopital.repository.rowmapper.MedecinRowMapper;
import com.gesthopital.service.EntityManager;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoin;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the Medecin entity.
 */
@SuppressWarnings("unused")
class MedecinRepositoryInternalImpl implements MedecinRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final MedecinRowMapper medecinMapper;

    private static final Table entityTable = Table.aliased("medecin", EntityManager.ENTITY_ALIAS);

    public MedecinRepositoryInternalImpl(R2dbcEntityTemplate template, EntityManager entityManager, MedecinRowMapper medecinMapper) {
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.medecinMapper = medecinMapper;
    }

    @Override
    public Flux<Medecin> findAllBy(Pageable pageable) {
        return findAllBy(pageable, null);
    }

    @Override
    public Flux<Medecin> findAllBy(Pageable pageable, Criteria criteria) {
        return createQuery(pageable, criteria).all();
    }

    RowsFetchSpec<Medecin> createQuery(Pageable pageable, Criteria criteria) {
        List<Expression> columns = MedecinSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        SelectFromAndJoin selectFrom = Select.builder().select(columns).from(entityTable);

        String select = entityManager.createSelect(selectFrom, Medecin.class, pageable, criteria);
        String alias = entityTable.getReferenceName().getReference();
        String selectWhere = Optional
            .ofNullable(criteria)
            .map(
                crit ->
                    new StringBuilder(select)
                        .append(" ")
                        .append("WHERE")
                        .append(" ")
                        .append(alias)
                        .append(".")
                        .append(crit.toString())
                        .toString()
            )
            .orElse(select); // TODO remove once https://github.com/spring-projects/spring-data-jdbc/issues/907 will be fixed
        return db.sql(selectWhere).map(this::process);
    }

    @Override
    public Flux<Medecin> findAll() {
        return findAllBy(null, null);
    }

    @Override
    public Mono<Medecin> findById(Long id) {
        return createQuery(null, where("id").is(id)).one();
    }

    private Medecin process(Row row, RowMetadata metadata) {
        Medecin entity = medecinMapper.apply(row, "e");
        return entity;
    }

    @Override
    public <S extends Medecin> Mono<S> insert(S entity) {
        return entityManager.insert(entity);
    }

    @Override
    public <S extends Medecin> Mono<S> save(S entity) {
        if (entity.getId() == null) {
            return insert(entity);
        } else {
            return update(entity)
                .map(
                    numberOfUpdates -> {
                        if (numberOfUpdates.intValue() <= 0) {
                            throw new IllegalStateException("Unable to update Medecin with id = " + entity.getId());
                        }
                        return entity;
                    }
                );
        }
    }

    @Override
    public Mono<Integer> update(Medecin entity) {
        //fixme is this the proper way?
        return r2dbcEntityTemplate.update(entity).thenReturn(1);
    }
}

class MedecinSqlHelper {

    static List<Expression> getColumns(Table table, String columnPrefix) {
        List<Expression> columns = new ArrayList<>();
        columns.add(Column.aliased("id", table, columnPrefix + "_id"));
        columns.add(Column.aliased("nom_complet_med", table, columnPrefix + "_nom_complet_med"));
        columns.add(Column.aliased("email_med", table, columnPrefix + "_email_med"));
        columns.add(Column.aliased("telephone_med", table, columnPrefix + "_telephone_med"));
        columns.add(Column.aliased("adresse_med", table, columnPrefix + "_adresse_med"));
        columns.add(Column.aliased("date_naissance_pat", table, columnPrefix + "_date_naissance_pat"));

        return columns;
    }
}
