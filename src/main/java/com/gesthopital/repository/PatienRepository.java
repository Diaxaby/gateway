package com.gesthopital.repository;

import com.gesthopital.domain.Patien;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Patien entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PatienRepository extends R2dbcRepository<Patien, Long>, PatienRepositoryInternal {
    Flux<Patien> findAllBy(Pageable pageable);

    @Override
    Mono<Patien> findOneWithEagerRelationships(Long id);

    @Override
    Flux<Patien> findAllWithEagerRelationships();

    @Override
    Flux<Patien> findAllWithEagerRelationships(Pageable page);

    @Override
    Mono<Void> deleteById(Long id);

    @Query(
        "SELECT entity.* FROM patien entity JOIN rel_patien__medecin joinTable ON entity.id = joinTable.patien_id WHERE joinTable.medecin_id = :id"
    )
    Flux<Patien> findByMedecin(Long id);

    // just to avoid having unambigous methods
    @Override
    Flux<Patien> findAll();

    @Override
    Mono<Patien> findById(Long id);

    @Override
    <S extends Patien> Mono<S> save(S entity);
}

interface PatienRepositoryInternal {
    <S extends Patien> Mono<S> insert(S entity);
    <S extends Patien> Mono<S> save(S entity);
    Mono<Integer> update(Patien entity);

    Flux<Patien> findAll();
    Mono<Patien> findById(Long id);
    Flux<Patien> findAllBy(Pageable pageable);
    Flux<Patien> findAllBy(Pageable pageable, Criteria criteria);

    Mono<Patien> findOneWithEagerRelationships(Long id);

    Flux<Patien> findAllWithEagerRelationships();

    Flux<Patien> findAllWithEagerRelationships(Pageable page);

    Mono<Void> deleteById(Long id);
}
