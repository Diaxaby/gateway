package com.gesthopital.repository.rowmapper;

import com.gesthopital.domain.Patien;
import com.gesthopital.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.LocalDate;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Patien}, with proper type conversions.
 */
@Service
public class PatienRowMapper implements BiFunction<Row, String, Patien> {

    private final ColumnConverter converter;

    public PatienRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Patien} stored in the database.
     */
    @Override
    public Patien apply(Row row, String prefix) {
        Patien entity = new Patien();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setNomCompletPat(converter.fromRow(row, prefix + "_nom_complet_pat", String.class));
        entity.setEmailPat(converter.fromRow(row, prefix + "_email_pat", String.class));
        entity.setTelephonePat(converter.fromRow(row, prefix + "_telephone_pat", String.class));
        entity.setAdressePat(converter.fromRow(row, prefix + "_adresse_pat", String.class));
        entity.setDateNaissancePat(converter.fromRow(row, prefix + "_date_naissance_pat", LocalDate.class));
        return entity;
    }
}
