package com.gesthopital.repository.rowmapper;

import com.gesthopital.domain.Medecin;
import com.gesthopital.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.time.LocalDate;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Medecin}, with proper type conversions.
 */
@Service
public class MedecinRowMapper implements BiFunction<Row, String, Medecin> {

    private final ColumnConverter converter;

    public MedecinRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Medecin} stored in the database.
     */
    @Override
    public Medecin apply(Row row, String prefix) {
        Medecin entity = new Medecin();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setNomCompletMed(converter.fromRow(row, prefix + "_nom_complet_med", String.class));
        entity.setEmailMed(converter.fromRow(row, prefix + "_email_med", String.class));
        entity.setTelephoneMed(converter.fromRow(row, prefix + "_telephone_med", String.class));
        entity.setAdresseMed(converter.fromRow(row, prefix + "_adresse_med", String.class));
        entity.setDateNaissancePat(converter.fromRow(row, prefix + "_date_naissance_pat", LocalDate.class));
        return entity;
    }
}
