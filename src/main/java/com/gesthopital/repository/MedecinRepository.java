package com.gesthopital.repository;

import com.gesthopital.domain.Medecin;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Medecin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MedecinRepository extends R2dbcRepository<Medecin, Long>, MedecinRepositoryInternal {
    Flux<Medecin> findAllBy(Pageable pageable);

    // just to avoid having unambigous methods
    @Override
    Flux<Medecin> findAll();

    @Override
    Mono<Medecin> findById(Long id);

    @Override
    <S extends Medecin> Mono<S> save(S entity);
}

interface MedecinRepositoryInternal {
    <S extends Medecin> Mono<S> insert(S entity);
    <S extends Medecin> Mono<S> save(S entity);
    Mono<Integer> update(Medecin entity);

    Flux<Medecin> findAll();
    Mono<Medecin> findById(Long id);
    Flux<Medecin> findAllBy(Pageable pageable);
    Flux<Medecin> findAllBy(Pageable pageable, Criteria criteria);
}
