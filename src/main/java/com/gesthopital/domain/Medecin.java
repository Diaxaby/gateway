package com.gesthopital.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Medecin.
 */
@Table("medecin")
public class Medecin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @NotNull(message = "must not be null")
    @Column("nom_complet_med")
    private String nomCompletMed;

    @NotNull(message = "must not be null")
    @Column("email_med")
    private String emailMed;

    @NotNull(message = "must not be null")
    @Column("telephone_med")
    private String telephoneMed;

    @Column("adresse_med")
    private String adresseMed;

    @NotNull(message = "must not be null")
    @Column("date_naissance_pat")
    private LocalDate dateNaissancePat;

    @JsonIgnoreProperties(value = { "medecins" }, allowSetters = true)
    @Transient
    private Set<Patien> patiens = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Medecin id(Long id) {
        this.id = id;
        return this;
    }

    public String getNomCompletMed() {
        return this.nomCompletMed;
    }

    public Medecin nomCompletMed(String nomCompletMed) {
        this.nomCompletMed = nomCompletMed;
        return this;
    }

    public void setNomCompletMed(String nomCompletMed) {
        this.nomCompletMed = nomCompletMed;
    }

    public String getEmailMed() {
        return this.emailMed;
    }

    public Medecin emailMed(String emailMed) {
        this.emailMed = emailMed;
        return this;
    }

    public void setEmailMed(String emailMed) {
        this.emailMed = emailMed;
    }

    public String getTelephoneMed() {
        return this.telephoneMed;
    }

    public Medecin telephoneMed(String telephoneMed) {
        this.telephoneMed = telephoneMed;
        return this;
    }

    public void setTelephoneMed(String telephoneMed) {
        this.telephoneMed = telephoneMed;
    }

    public String getAdresseMed() {
        return this.adresseMed;
    }

    public Medecin adresseMed(String adresseMed) {
        this.adresseMed = adresseMed;
        return this;
    }

    public void setAdresseMed(String adresseMed) {
        this.adresseMed = adresseMed;
    }

    public LocalDate getDateNaissancePat() {
        return this.dateNaissancePat;
    }

    public Medecin dateNaissancePat(LocalDate dateNaissancePat) {
        this.dateNaissancePat = dateNaissancePat;
        return this;
    }

    public void setDateNaissancePat(LocalDate dateNaissancePat) {
        this.dateNaissancePat = dateNaissancePat;
    }

    public Set<Patien> getPatiens() {
        return this.patiens;
    }

    public Medecin patiens(Set<Patien> patiens) {
        this.setPatiens(patiens);
        return this;
    }

    public Medecin addPatien(Patien patien) {
        this.patiens.add(patien);
        patien.getMedecins().add(this);
        return this;
    }

    public Medecin removePatien(Patien patien) {
        this.patiens.remove(patien);
        patien.getMedecins().remove(this);
        return this;
    }

    public void setPatiens(Set<Patien> patiens) {
        if (this.patiens != null) {
            this.patiens.forEach(i -> i.removeMedecin(this));
        }
        if (patiens != null) {
            patiens.forEach(i -> i.addMedecin(this));
        }
        this.patiens = patiens;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Medecin)) {
            return false;
        }
        return id != null && id.equals(((Medecin) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Medecin{" +
            "id=" + getId() +
            ", nomCompletMed='" + getNomCompletMed() + "'" +
            ", emailMed='" + getEmailMed() + "'" +
            ", telephoneMed='" + getTelephoneMed() + "'" +
            ", adresseMed='" + getAdresseMed() + "'" +
            ", dateNaissancePat='" + getDateNaissancePat() + "'" +
            "}";
    }
}
