package com.gesthopital.web.rest;

import com.gesthopital.domain.Patien;
import com.gesthopital.repository.PatienRepository;
import com.gesthopital.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.gesthopital.domain.Patien}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PatienResource {

    private final Logger log = LoggerFactory.getLogger(PatienResource.class);

    private static final String ENTITY_NAME = "patien";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PatienRepository patienRepository;

    public PatienResource(PatienRepository patienRepository) {
        this.patienRepository = patienRepository;
    }

    /**
     * {@code POST  /patiens} : Create a new patien.
     *
     * @param patien the patien to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new patien, or with status {@code 400 (Bad Request)} if the patien has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/patiens")
    public Mono<ResponseEntity<Patien>> createPatien(@Valid @RequestBody Patien patien) throws URISyntaxException {
        log.debug("REST request to save Patien : {}", patien);
        if (patien.getId() != null) {
            throw new BadRequestAlertException("A new patien cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return patienRepository
            .save(patien)
            .map(
                result -> {
                    try {
                        return ResponseEntity
                            .created(new URI("/api/patiens/" + result.getId()))
                            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result);
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                }
            );
    }

    /**
     * {@code PUT  /patiens/:id} : Updates an existing patien.
     *
     * @param id the id of the patien to save.
     * @param patien the patien to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated patien,
     * or with status {@code 400 (Bad Request)} if the patien is not valid,
     * or with status {@code 500 (Internal Server Error)} if the patien couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/patiens/{id}")
    public Mono<ResponseEntity<Patien>> updatePatien(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Patien patien
    ) throws URISyntaxException {
        log.debug("REST request to update Patien : {}, {}", id, patien);
        if (patien.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, patien.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return patienRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    return patienRepository
                        .save(patien)
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            result ->
                                ResponseEntity
                                    .ok()
                                    .headers(
                                        HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString())
                                    )
                                    .body(result)
                        );
                }
            );
    }

    /**
     * {@code PATCH  /patiens/:id} : Partial updates given fields of an existing patien, field will ignore if it is null
     *
     * @param id the id of the patien to save.
     * @param patien the patien to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated patien,
     * or with status {@code 400 (Bad Request)} if the patien is not valid,
     * or with status {@code 404 (Not Found)} if the patien is not found,
     * or with status {@code 500 (Internal Server Error)} if the patien couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/patiens/{id}", consumes = "application/merge-patch+json")
    public Mono<ResponseEntity<Patien>> partialUpdatePatien(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Patien patien
    ) throws URISyntaxException {
        log.debug("REST request to partial update Patien partially : {}, {}", id, patien);
        if (patien.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, patien.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return patienRepository
            .existsById(id)
            .flatMap(
                exists -> {
                    if (!exists) {
                        return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                    }

                    Mono<Patien> result = patienRepository
                        .findById(patien.getId())
                        .map(
                            existingPatien -> {
                                if (patien.getNomCompletPat() != null) {
                                    existingPatien.setNomCompletPat(patien.getNomCompletPat());
                                }
                                if (patien.getEmailPat() != null) {
                                    existingPatien.setEmailPat(patien.getEmailPat());
                                }
                                if (patien.getTelephonePat() != null) {
                                    existingPatien.setTelephonePat(patien.getTelephonePat());
                                }
                                if (patien.getAdressePat() != null) {
                                    existingPatien.setAdressePat(patien.getAdressePat());
                                }
                                if (patien.getDateNaissancePat() != null) {
                                    existingPatien.setDateNaissancePat(patien.getDateNaissancePat());
                                }

                                return existingPatien;
                            }
                        )
                        .flatMap(patienRepository::save);

                    return result
                        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                        .map(
                            res ->
                                ResponseEntity
                                    .ok()
                                    .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                                    .body(res)
                        );
                }
            );
    }

    /**
     * {@code GET  /patiens} : get all the patiens.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of patiens in body.
     */
    @GetMapping("/patiens")
    public Mono<ResponseEntity<List<Patien>>> getAllPatiens(
        Pageable pageable,
        ServerHttpRequest request,
        @RequestParam(required = false, defaultValue = "false") boolean eagerload
    ) {
        log.debug("REST request to get a page of Patiens");
        return patienRepository
            .count()
            .zipWith(patienRepository.findAllBy(pageable).collectList())
            .map(
                countWithEntities -> {
                    return ResponseEntity
                        .ok()
                        .headers(
                            PaginationUtil.generatePaginationHttpHeaders(
                                UriComponentsBuilder.fromHttpRequest(request),
                                new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                            )
                        )
                        .body(countWithEntities.getT2());
                }
            );
    }

    /**
     * {@code GET  /patiens/:id} : get the "id" patien.
     *
     * @param id the id of the patien to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the patien, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/patiens/{id}")
    public Mono<ResponseEntity<Patien>> getPatien(@PathVariable Long id) {
        log.debug("REST request to get Patien : {}", id);
        Mono<Patien> patien = patienRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(patien);
    }

    /**
     * {@code DELETE  /patiens/:id} : delete the "id" patien.
     *
     * @param id the id of the patien to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/patiens/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deletePatien(@PathVariable Long id) {
        log.debug("REST request to delete Patien : {}", id);
        return patienRepository
            .deleteById(id)
            .map(
                result ->
                    ResponseEntity
                        .noContent()
                        .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                        .build()
            );
    }
}
