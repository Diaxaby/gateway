package com.gesthopital.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.gesthopital.IntegrationTest;
import com.gesthopital.domain.Patien;
import com.gesthopital.repository.PatienRepository;
import com.gesthopital.service.EntityManager;
import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link PatienResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class PatienResourceIT {

    private static final String DEFAULT_NOM_COMPLET_PAT = "AAAAAAAAAA";
    private static final String UPDATED_NOM_COMPLET_PAT = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_PAT = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_PAT = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE_PAT = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE_PAT = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESSE_PAT = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE_PAT = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_NAISSANCE_PAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_NAISSANCE_PAT = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/patiens";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PatienRepository patienRepository;

    @Mock
    private PatienRepository patienRepositoryMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Patien patien;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Patien createEntity(EntityManager em) {
        Patien patien = new Patien()
            .nomCompletPat(DEFAULT_NOM_COMPLET_PAT)
            .emailPat(DEFAULT_EMAIL_PAT)
            .telephonePat(DEFAULT_TELEPHONE_PAT)
            .adressePat(DEFAULT_ADRESSE_PAT)
            .dateNaissancePat(DEFAULT_DATE_NAISSANCE_PAT);
        return patien;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Patien createUpdatedEntity(EntityManager em) {
        Patien patien = new Patien()
            .nomCompletPat(UPDATED_NOM_COMPLET_PAT)
            .emailPat(UPDATED_EMAIL_PAT)
            .telephonePat(UPDATED_TELEPHONE_PAT)
            .adressePat(UPDATED_ADRESSE_PAT)
            .dateNaissancePat(UPDATED_DATE_NAISSANCE_PAT);
        return patien;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll("rel_patien__medecin").block();
            em.deleteAll(Patien.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        patien = createEntity(em);
    }

    @Test
    void createPatien() throws Exception {
        int databaseSizeBeforeCreate = patienRepository.findAll().collectList().block().size();
        // Create the Patien
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(patien))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeCreate + 1);
        Patien testPatien = patienList.get(patienList.size() - 1);
        assertThat(testPatien.getNomCompletPat()).isEqualTo(DEFAULT_NOM_COMPLET_PAT);
        assertThat(testPatien.getEmailPat()).isEqualTo(DEFAULT_EMAIL_PAT);
        assertThat(testPatien.getTelephonePat()).isEqualTo(DEFAULT_TELEPHONE_PAT);
        assertThat(testPatien.getAdressePat()).isEqualTo(DEFAULT_ADRESSE_PAT);
        assertThat(testPatien.getDateNaissancePat()).isEqualTo(DEFAULT_DATE_NAISSANCE_PAT);
    }

    @Test
    void createPatienWithExistingId() throws Exception {
        // Create the Patien with an existing ID
        patien.setId(1L);

        int databaseSizeBeforeCreate = patienRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(patien))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkNomCompletPatIsRequired() throws Exception {
        int databaseSizeBeforeTest = patienRepository.findAll().collectList().block().size();
        // set the field null
        patien.setNomCompletPat(null);

        // Create the Patien, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(patien))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkEmailPatIsRequired() throws Exception {
        int databaseSizeBeforeTest = patienRepository.findAll().collectList().block().size();
        // set the field null
        patien.setEmailPat(null);

        // Create the Patien, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(patien))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkTelephonePatIsRequired() throws Exception {
        int databaseSizeBeforeTest = patienRepository.findAll().collectList().block().size();
        // set the field null
        patien.setTelephonePat(null);

        // Create the Patien, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(patien))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkDateNaissancePatIsRequired() throws Exception {
        int databaseSizeBeforeTest = patienRepository.findAll().collectList().block().size();
        // set the field null
        patien.setDateNaissancePat(null);

        // Create the Patien, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(patien))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllPatiens() {
        // Initialize the database
        patienRepository.save(patien).block();

        // Get all the patienList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(patien.getId().intValue()))
            .jsonPath("$.[*].nomCompletPat")
            .value(hasItem(DEFAULT_NOM_COMPLET_PAT))
            .jsonPath("$.[*].emailPat")
            .value(hasItem(DEFAULT_EMAIL_PAT))
            .jsonPath("$.[*].telephonePat")
            .value(hasItem(DEFAULT_TELEPHONE_PAT))
            .jsonPath("$.[*].adressePat")
            .value(hasItem(DEFAULT_ADRESSE_PAT))
            .jsonPath("$.[*].dateNaissancePat")
            .value(hasItem(DEFAULT_DATE_NAISSANCE_PAT.toString()));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllPatiensWithEagerRelationshipsIsEnabled() {
        when(patienRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(Flux.empty());

        webTestClient.get().uri(ENTITY_API_URL + "?eagerload=true").exchange().expectStatus().isOk();

        verify(patienRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllPatiensWithEagerRelationshipsIsNotEnabled() {
        when(patienRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(Flux.empty());

        webTestClient.get().uri(ENTITY_API_URL + "?eagerload=true").exchange().expectStatus().isOk();

        verify(patienRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    void getPatien() {
        // Initialize the database
        patienRepository.save(patien).block();

        // Get the patien
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, patien.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(patien.getId().intValue()))
            .jsonPath("$.nomCompletPat")
            .value(is(DEFAULT_NOM_COMPLET_PAT))
            .jsonPath("$.emailPat")
            .value(is(DEFAULT_EMAIL_PAT))
            .jsonPath("$.telephonePat")
            .value(is(DEFAULT_TELEPHONE_PAT))
            .jsonPath("$.adressePat")
            .value(is(DEFAULT_ADRESSE_PAT))
            .jsonPath("$.dateNaissancePat")
            .value(is(DEFAULT_DATE_NAISSANCE_PAT.toString()));
    }

    @Test
    void getNonExistingPatien() {
        // Get the patien
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewPatien() throws Exception {
        // Initialize the database
        patienRepository.save(patien).block();

        int databaseSizeBeforeUpdate = patienRepository.findAll().collectList().block().size();

        // Update the patien
        Patien updatedPatien = patienRepository.findById(patien.getId()).block();
        updatedPatien
            .nomCompletPat(UPDATED_NOM_COMPLET_PAT)
            .emailPat(UPDATED_EMAIL_PAT)
            .telephonePat(UPDATED_TELEPHONE_PAT)
            .adressePat(UPDATED_ADRESSE_PAT)
            .dateNaissancePat(UPDATED_DATE_NAISSANCE_PAT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedPatien.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedPatien))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
        Patien testPatien = patienList.get(patienList.size() - 1);
        assertThat(testPatien.getNomCompletPat()).isEqualTo(UPDATED_NOM_COMPLET_PAT);
        assertThat(testPatien.getEmailPat()).isEqualTo(UPDATED_EMAIL_PAT);
        assertThat(testPatien.getTelephonePat()).isEqualTo(UPDATED_TELEPHONE_PAT);
        assertThat(testPatien.getAdressePat()).isEqualTo(UPDATED_ADRESSE_PAT);
        assertThat(testPatien.getDateNaissancePat()).isEqualTo(UPDATED_DATE_NAISSANCE_PAT);
    }

    @Test
    void putNonExistingPatien() throws Exception {
        int databaseSizeBeforeUpdate = patienRepository.findAll().collectList().block().size();
        patien.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, patien.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(patien))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchPatien() throws Exception {
        int databaseSizeBeforeUpdate = patienRepository.findAll().collectList().block().size();
        patien.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(patien))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamPatien() throws Exception {
        int databaseSizeBeforeUpdate = patienRepository.findAll().collectList().block().size();
        patien.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(patien))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdatePatienWithPatch() throws Exception {
        // Initialize the database
        patienRepository.save(patien).block();

        int databaseSizeBeforeUpdate = patienRepository.findAll().collectList().block().size();

        // Update the patien using partial update
        Patien partialUpdatedPatien = new Patien();
        partialUpdatedPatien.setId(patien.getId());

        partialUpdatedPatien.dateNaissancePat(UPDATED_DATE_NAISSANCE_PAT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPatien.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPatien))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
        Patien testPatien = patienList.get(patienList.size() - 1);
        assertThat(testPatien.getNomCompletPat()).isEqualTo(DEFAULT_NOM_COMPLET_PAT);
        assertThat(testPatien.getEmailPat()).isEqualTo(DEFAULT_EMAIL_PAT);
        assertThat(testPatien.getTelephonePat()).isEqualTo(DEFAULT_TELEPHONE_PAT);
        assertThat(testPatien.getAdressePat()).isEqualTo(DEFAULT_ADRESSE_PAT);
        assertThat(testPatien.getDateNaissancePat()).isEqualTo(UPDATED_DATE_NAISSANCE_PAT);
    }

    @Test
    void fullUpdatePatienWithPatch() throws Exception {
        // Initialize the database
        patienRepository.save(patien).block();

        int databaseSizeBeforeUpdate = patienRepository.findAll().collectList().block().size();

        // Update the patien using partial update
        Patien partialUpdatedPatien = new Patien();
        partialUpdatedPatien.setId(patien.getId());

        partialUpdatedPatien
            .nomCompletPat(UPDATED_NOM_COMPLET_PAT)
            .emailPat(UPDATED_EMAIL_PAT)
            .telephonePat(UPDATED_TELEPHONE_PAT)
            .adressePat(UPDATED_ADRESSE_PAT)
            .dateNaissancePat(UPDATED_DATE_NAISSANCE_PAT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPatien.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPatien))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
        Patien testPatien = patienList.get(patienList.size() - 1);
        assertThat(testPatien.getNomCompletPat()).isEqualTo(UPDATED_NOM_COMPLET_PAT);
        assertThat(testPatien.getEmailPat()).isEqualTo(UPDATED_EMAIL_PAT);
        assertThat(testPatien.getTelephonePat()).isEqualTo(UPDATED_TELEPHONE_PAT);
        assertThat(testPatien.getAdressePat()).isEqualTo(UPDATED_ADRESSE_PAT);
        assertThat(testPatien.getDateNaissancePat()).isEqualTo(UPDATED_DATE_NAISSANCE_PAT);
    }

    @Test
    void patchNonExistingPatien() throws Exception {
        int databaseSizeBeforeUpdate = patienRepository.findAll().collectList().block().size();
        patien.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, patien.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(patien))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchPatien() throws Exception {
        int databaseSizeBeforeUpdate = patienRepository.findAll().collectList().block().size();
        patien.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(patien))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamPatien() throws Exception {
        int databaseSizeBeforeUpdate = patienRepository.findAll().collectList().block().size();
        patien.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(patien))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Patien in the database
        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deletePatien() {
        // Initialize the database
        patienRepository.save(patien).block();

        int databaseSizeBeforeDelete = patienRepository.findAll().collectList().block().size();

        // Delete the patien
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, patien.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Patien> patienList = patienRepository.findAll().collectList().block();
        assertThat(patienList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
