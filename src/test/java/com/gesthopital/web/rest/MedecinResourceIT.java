package com.gesthopital.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import com.gesthopital.IntegrationTest;
import com.gesthopital.domain.Medecin;
import com.gesthopital.repository.MedecinRepository;
import com.gesthopital.service.EntityManager;
import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link MedecinResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class MedecinResourceIT {

    private static final String DEFAULT_NOM_COMPLET_MED = "AAAAAAAAAA";
    private static final String UPDATED_NOM_COMPLET_MED = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_MED = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_MED = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE_MED = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE_MED = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESSE_MED = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE_MED = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_NAISSANCE_PAT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_NAISSANCE_PAT = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/medecins";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MedecinRepository medecinRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Medecin medecin;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Medecin createEntity(EntityManager em) {
        Medecin medecin = new Medecin()
            .nomCompletMed(DEFAULT_NOM_COMPLET_MED)
            .emailMed(DEFAULT_EMAIL_MED)
            .telephoneMed(DEFAULT_TELEPHONE_MED)
            .adresseMed(DEFAULT_ADRESSE_MED)
            .dateNaissancePat(DEFAULT_DATE_NAISSANCE_PAT);
        return medecin;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Medecin createUpdatedEntity(EntityManager em) {
        Medecin medecin = new Medecin()
            .nomCompletMed(UPDATED_NOM_COMPLET_MED)
            .emailMed(UPDATED_EMAIL_MED)
            .telephoneMed(UPDATED_TELEPHONE_MED)
            .adresseMed(UPDATED_ADRESSE_MED)
            .dateNaissancePat(UPDATED_DATE_NAISSANCE_PAT);
        return medecin;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Medecin.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        medecin = createEntity(em);
    }

    @Test
    void createMedecin() throws Exception {
        int databaseSizeBeforeCreate = medecinRepository.findAll().collectList().block().size();
        // Create the Medecin
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medecin))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeCreate + 1);
        Medecin testMedecin = medecinList.get(medecinList.size() - 1);
        assertThat(testMedecin.getNomCompletMed()).isEqualTo(DEFAULT_NOM_COMPLET_MED);
        assertThat(testMedecin.getEmailMed()).isEqualTo(DEFAULT_EMAIL_MED);
        assertThat(testMedecin.getTelephoneMed()).isEqualTo(DEFAULT_TELEPHONE_MED);
        assertThat(testMedecin.getAdresseMed()).isEqualTo(DEFAULT_ADRESSE_MED);
        assertThat(testMedecin.getDateNaissancePat()).isEqualTo(DEFAULT_DATE_NAISSANCE_PAT);
    }

    @Test
    void createMedecinWithExistingId() throws Exception {
        // Create the Medecin with an existing ID
        medecin.setId(1L);

        int databaseSizeBeforeCreate = medecinRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medecin))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkNomCompletMedIsRequired() throws Exception {
        int databaseSizeBeforeTest = medecinRepository.findAll().collectList().block().size();
        // set the field null
        medecin.setNomCompletMed(null);

        // Create the Medecin, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medecin))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkEmailMedIsRequired() throws Exception {
        int databaseSizeBeforeTest = medecinRepository.findAll().collectList().block().size();
        // set the field null
        medecin.setEmailMed(null);

        // Create the Medecin, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medecin))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkTelephoneMedIsRequired() throws Exception {
        int databaseSizeBeforeTest = medecinRepository.findAll().collectList().block().size();
        // set the field null
        medecin.setTelephoneMed(null);

        // Create the Medecin, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medecin))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkDateNaissancePatIsRequired() throws Exception {
        int databaseSizeBeforeTest = medecinRepository.findAll().collectList().block().size();
        // set the field null
        medecin.setDateNaissancePat(null);

        // Create the Medecin, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medecin))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllMedecins() {
        // Initialize the database
        medecinRepository.save(medecin).block();

        // Get all the medecinList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(medecin.getId().intValue()))
            .jsonPath("$.[*].nomCompletMed")
            .value(hasItem(DEFAULT_NOM_COMPLET_MED))
            .jsonPath("$.[*].emailMed")
            .value(hasItem(DEFAULT_EMAIL_MED))
            .jsonPath("$.[*].telephoneMed")
            .value(hasItem(DEFAULT_TELEPHONE_MED))
            .jsonPath("$.[*].adresseMed")
            .value(hasItem(DEFAULT_ADRESSE_MED))
            .jsonPath("$.[*].dateNaissancePat")
            .value(hasItem(DEFAULT_DATE_NAISSANCE_PAT.toString()));
    }

    @Test
    void getMedecin() {
        // Initialize the database
        medecinRepository.save(medecin).block();

        // Get the medecin
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, medecin.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(medecin.getId().intValue()))
            .jsonPath("$.nomCompletMed")
            .value(is(DEFAULT_NOM_COMPLET_MED))
            .jsonPath("$.emailMed")
            .value(is(DEFAULT_EMAIL_MED))
            .jsonPath("$.telephoneMed")
            .value(is(DEFAULT_TELEPHONE_MED))
            .jsonPath("$.adresseMed")
            .value(is(DEFAULT_ADRESSE_MED))
            .jsonPath("$.dateNaissancePat")
            .value(is(DEFAULT_DATE_NAISSANCE_PAT.toString()));
    }

    @Test
    void getNonExistingMedecin() {
        // Get the medecin
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewMedecin() throws Exception {
        // Initialize the database
        medecinRepository.save(medecin).block();

        int databaseSizeBeforeUpdate = medecinRepository.findAll().collectList().block().size();

        // Update the medecin
        Medecin updatedMedecin = medecinRepository.findById(medecin.getId()).block();
        updatedMedecin
            .nomCompletMed(UPDATED_NOM_COMPLET_MED)
            .emailMed(UPDATED_EMAIL_MED)
            .telephoneMed(UPDATED_TELEPHONE_MED)
            .adresseMed(UPDATED_ADRESSE_MED)
            .dateNaissancePat(UPDATED_DATE_NAISSANCE_PAT);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedMedecin.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedMedecin))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
        Medecin testMedecin = medecinList.get(medecinList.size() - 1);
        assertThat(testMedecin.getNomCompletMed()).isEqualTo(UPDATED_NOM_COMPLET_MED);
        assertThat(testMedecin.getEmailMed()).isEqualTo(UPDATED_EMAIL_MED);
        assertThat(testMedecin.getTelephoneMed()).isEqualTo(UPDATED_TELEPHONE_MED);
        assertThat(testMedecin.getAdresseMed()).isEqualTo(UPDATED_ADRESSE_MED);
        assertThat(testMedecin.getDateNaissancePat()).isEqualTo(UPDATED_DATE_NAISSANCE_PAT);
    }

    @Test
    void putNonExistingMedecin() throws Exception {
        int databaseSizeBeforeUpdate = medecinRepository.findAll().collectList().block().size();
        medecin.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, medecin.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medecin))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchMedecin() throws Exception {
        int databaseSizeBeforeUpdate = medecinRepository.findAll().collectList().block().size();
        medecin.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medecin))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamMedecin() throws Exception {
        int databaseSizeBeforeUpdate = medecinRepository.findAll().collectList().block().size();
        medecin.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(medecin))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateMedecinWithPatch() throws Exception {
        // Initialize the database
        medecinRepository.save(medecin).block();

        int databaseSizeBeforeUpdate = medecinRepository.findAll().collectList().block().size();

        // Update the medecin using partial update
        Medecin partialUpdatedMedecin = new Medecin();
        partialUpdatedMedecin.setId(medecin.getId());

        partialUpdatedMedecin
            .nomCompletMed(UPDATED_NOM_COMPLET_MED)
            .emailMed(UPDATED_EMAIL_MED)
            .telephoneMed(UPDATED_TELEPHONE_MED)
            .dateNaissancePat(UPDATED_DATE_NAISSANCE_PAT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedMedecin.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedMedecin))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
        Medecin testMedecin = medecinList.get(medecinList.size() - 1);
        assertThat(testMedecin.getNomCompletMed()).isEqualTo(UPDATED_NOM_COMPLET_MED);
        assertThat(testMedecin.getEmailMed()).isEqualTo(UPDATED_EMAIL_MED);
        assertThat(testMedecin.getTelephoneMed()).isEqualTo(UPDATED_TELEPHONE_MED);
        assertThat(testMedecin.getAdresseMed()).isEqualTo(DEFAULT_ADRESSE_MED);
        assertThat(testMedecin.getDateNaissancePat()).isEqualTo(UPDATED_DATE_NAISSANCE_PAT);
    }

    @Test
    void fullUpdateMedecinWithPatch() throws Exception {
        // Initialize the database
        medecinRepository.save(medecin).block();

        int databaseSizeBeforeUpdate = medecinRepository.findAll().collectList().block().size();

        // Update the medecin using partial update
        Medecin partialUpdatedMedecin = new Medecin();
        partialUpdatedMedecin.setId(medecin.getId());

        partialUpdatedMedecin
            .nomCompletMed(UPDATED_NOM_COMPLET_MED)
            .emailMed(UPDATED_EMAIL_MED)
            .telephoneMed(UPDATED_TELEPHONE_MED)
            .adresseMed(UPDATED_ADRESSE_MED)
            .dateNaissancePat(UPDATED_DATE_NAISSANCE_PAT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedMedecin.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedMedecin))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
        Medecin testMedecin = medecinList.get(medecinList.size() - 1);
        assertThat(testMedecin.getNomCompletMed()).isEqualTo(UPDATED_NOM_COMPLET_MED);
        assertThat(testMedecin.getEmailMed()).isEqualTo(UPDATED_EMAIL_MED);
        assertThat(testMedecin.getTelephoneMed()).isEqualTo(UPDATED_TELEPHONE_MED);
        assertThat(testMedecin.getAdresseMed()).isEqualTo(UPDATED_ADRESSE_MED);
        assertThat(testMedecin.getDateNaissancePat()).isEqualTo(UPDATED_DATE_NAISSANCE_PAT);
    }

    @Test
    void patchNonExistingMedecin() throws Exception {
        int databaseSizeBeforeUpdate = medecinRepository.findAll().collectList().block().size();
        medecin.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, medecin.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(medecin))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchMedecin() throws Exception {
        int databaseSizeBeforeUpdate = medecinRepository.findAll().collectList().block().size();
        medecin.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(medecin))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamMedecin() throws Exception {
        int databaseSizeBeforeUpdate = medecinRepository.findAll().collectList().block().size();
        medecin.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(medecin))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Medecin in the database
        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteMedecin() {
        // Initialize the database
        medecinRepository.save(medecin).block();

        int databaseSizeBeforeDelete = medecinRepository.findAll().collectList().block().size();

        // Delete the medecin
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, medecin.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Medecin> medecinList = medecinRepository.findAll().collectList().block();
        assertThat(medecinList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
